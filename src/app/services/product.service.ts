import { Product } from './../model/product';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  readonly PRODUCT_ROOT_URL = 'http://localhost:8080/api/products/';

  constructor(private http: HttpClient) {
  }

  getProducts() {
    let url = this.PRODUCT_ROOT_URL;
    return this.http.get<Product[]>(url);
  }

  getProductById(id: number) {
    let url = this.PRODUCT_ROOT_URL + id;
    return this.http.get<Product>(url);
  }

  addProduct(product: Product) {
    let url = this.PRODUCT_ROOT_URL;
    this.http.post(url, product).subscribe();
  }

  updateProduct(id: number, product: Product) {
    let url = this.PRODUCT_ROOT_URL + id;
    this.http.put(url, product).subscribe();
  }

  deleteProduct(id: number) {
    let url = `${this.PRODUCT_ROOT_URL}${id}`;
    this.http.delete(url).subscribe();
  }
}
