import { DataSource } from '@angular/cdk/table';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Product } from '../model/product';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: any;
  @ViewChild(MatSort) sort: MatSort = new MatSort;
  datasource: any;
  displayedColumns: string[] = ['id', 'name', 'price', 'quantity']
  products: Product[] = [];

  constructor(private productService: ProductService) {
    this.productService.getProducts()
      .subscribe(products => {
        this.datasource = new MatTableDataSource<Product>(products);
        this.datasource.paginator = this.paginator;
        this.datasource.sort = this.sort;
      });
  }

  deleteProduct(id: number) {
    this.productService.deleteProduct(id);
    this.productService.getProducts()
      .subscribe(result => { this.products = result });
  }

  ngOnInit(): void {
  }

}
