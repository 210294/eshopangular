import { ProductService } from './../services/product.service';
import { Product } from './../model/product';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent implements OnInit {
  product: Product = new Product();

  constructor(
    private route: ActivatedRoute,
    private productService: ProductService) {

    let id = this.route.snapshot.paramMap.get('id');
    if (id) this.productService.getProductById(+id)
      .subscribe(result => this.product = result);

  }

  updateProduct() {
    if (this.product.id) this.productService.updateProduct(this.product.id, this.product)
  }

  addProduct() {
    this.productService.addProduct(this.product)
  }

  ngOnInit(): void {
  }

}
